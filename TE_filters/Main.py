#%% 
# Import lib
import numpy as np
from scipy.ndimage import gaussian_filter1d
from scipy.signal import convolve
from MFsynthesis import synthmrw,synthMultiFractalField
import entropy.entropy as entropy # Assuming your TE library is imported
import entropy.tools as tools
import matplotlib.pyplot as plt
import os 
import pickle
#%% 
#Saving routine


save_dir = '/users2/local/e22froge/codes/TE_Filter/Results'
os.makedirs(save_dir, exist_ok=True)

def save_results(key, te_values, te_std):
    filename = f'{save_dir}/result_{key[0]}_{key[1]}_{key[2]}_{key[3]}_{key[4]}.pkl'
    with open(filename, 'wb') as f:
        pickle.dump((te_values, te_std), f)

def load_results(key):
    filename = f'{save_dir}/result_{key[0]}_{key[1]}_{key[2]}_{key[3]}_{key[4]}.pkl'
    if os.path.exists(filename):
        with open(filename, 'rb') as f:
            return pickle.load(f)
    return None


#%% 
# Define parameters, kernels and keys
entropy.set_sampling(Theiler=3,N_eff=10000,N_real=10)

# Configuration
N = 2**20
path = '/users2/local/e22froge/codes/Innovation/DataSignals/uspa_mob15_1.dat'  # path to data
fid = open(path, 'r')
Modane = np.fromfile(fid, dtype='>f')[:N]

# Generate synthetic FBM signal (remove FGN if unnecessary)
fbm_signal,_ = synthmrw(dim=1, N=N, H=1/3, lambda2=0)  # Placeholder for FBM
fbm_signal=fbm_signal[:,0]
fgn_signal = np.diff(fbm_signal, axis=0)

FBM_regu=synthMultiFractalField(dim=1, N=N, H=1/3, lambda2=0, regparam=[1, 2980])
FBM_regu=FBM_regu[:,0]


scales = [20, 30]  # Define your scales
positions = ['causal', 'symmetric', 'anticausal']
signals=['fgn_signal', 'fbm_signal', 'Modane']
max_lag_factor = 2  # We will vary lag up to 10 times the largest scale

# fbm_signal_flip=np.flip(fbm_signal)
# fgn_signal_flip=np.flip(fgn_signal)
# Modane_flip=np.flip(Modane)
# flipped_signals=['fgn_signal_flip','fbm_signal_flip','Modane_flip']

# Prepare results dictionary
results = {}

def gaussian_kernel(scale, position):
    size=10*scale+1
    x = np.linspace(-(size//2), size // 2, size)
    kernel = np.exp(-0.5 * (x / scale) ** 2)
    
    if position == 'causal':
        kernel[:size//2] = 0  

    elif position == 'anticausal':
        kernel[size//2+1:] = 0  

    elif position == 'symmetric':
        pass

    kernel /= kernel.sum()
    return kernel

def gaussian_HP_kernel(scale, position):
    size=10*scale+1
    x = np.linspace(-(size//2), size // 2, size)
    kernel = np.exp(-0.5 * (x / scale) ** 2)

    if position == 'causal':
        kernel[:size//2] = 0  

    elif position == 'anticausal':
        kernel[size//2+1:] = 0  

    elif position == 'symmetric':
        pass
    

    kernel /= kernel.sum()
    delta=np.zeros(size)
    delta[size//2]=1
    kernel=delta-kernel
    return kernel



def box_HP_kernel(scale, position):
    size=10*scale+1
    x = np.linspace(-(size//2), size // 2, size)
    kernel = np.zeros(size)
    kernel[np.where(np.abs(x)<=scale)]=1
    if position == 'causal':
        kernel[:size//2] = 0  

    elif position == 'anticausal':
        kernel[size//2+1:] = 0  

    elif position == 'symmetric':
        pass
    

    kernel /= kernel.sum()
    delta=np.zeros(size)
    delta[size//2]=1
    kernel=delta-kernel
    return kernel


def gaussian_BP_kernel(scale_high,position,scale_low=None):
    if scale_low==None:
        scale_low=2*scale_high
    size=10*scale_low+1
    x = np.linspace(-(size//2), size // 2, size)
    kernel_low = np.exp(-0.5 * (x / scale_low) ** 2)
    kernel_high = np.exp(-0.5 * (x / scale_high) ** 2)
    if position == 'causal':
        kernel_low[:size//2] = 0  
        kernel_high[:size//2] = 0  

    elif position == 'anticausal':
        kernel_low[size//2 + 1:] = 0  
        kernel_high[size//2 + 1:] = 0  

    elif position == 'symmetric':
        pass

    kernel_low /= kernel_low.sum()
    kernel_high /= kernel_high.sum()

    kernel=kernel_high-kernel_low
    return kernel

def box_BP_kernel(scale_high,position,scale_low=None ):
    if scale_low==None:
        scale_low=2*scale_high
    size=10*scale_low+1
    x = np.linspace(-(size//2), size // 2, size)
    kernel_low = np.zeros(size)
    kernel_low[np.where(np.abs(x)<=scale_low)]=1
    kernel_high = np.zeros(size)
    kernel_high[np.where(np.abs(x)<=scale_high)]=1
    if position == 'causal':
        kernel_low[:size//2] = 0  
        kernel_high[:size//2] = 0  

    elif position == 'anticausal':
        kernel_low[size//2 + 1:] = 0  
        kernel_high[size//2 + 1:] = 0  

    elif position == 'symmetric':
        pass

    kernel_low /= kernel_low.sum()
    kernel_high /= kernel_high.sum()

    kernel=kernel=kernel_high-kernel_low
    return kernel


def increment_kernel(scale,position):
    if position == 'causal':
        kernel = np.zeros(2 * scale + 1)
        kernel[scale] = 1 
        kernel[-1] = -1     
  

    elif position == 'anticausal':
        kernel = np.zeros(2 * scale + 1)
        kernel[scale] = -1      
        kernel[0] = 1    
 

    elif position == 'symmetric':
        kernel = np.zeros(2 * scale + 1)
        kernel[scale // 2] = 1 
        kernel[-(scale // 2) - 1] = -1  

    return kernel

def gabor_kernel(scale, position,omega=None):
    if omega==None:
        omega=5/scale
    size=10*scale+1
    x = np.linspace(-(size//2), size // 2, size)
    
    
    if position == 'causal':
        kernel = np.where(x >= 0, (1 / scale) * np.exp(-x / scale), 0)  

    elif position == 'anticausal':
        kernel = np.where(x <= 0, (1 / scale) * np.exp(x / scale), 0)  

    elif position == 'symmetric':
        kernel = np.exp(-0.5 * (x / scale) ** 2)
    kernel/= kernel.sum()
    kernel=kernel* np.cos(omega * x)
    return kernel

def box_kernel(scale, position):
    size=10*scale+1
    x = np.linspace(-(size//2), size // 2, size)
    kernel = np.zeros(size)
    kernel[np.where(np.abs(x)<=scale)]=1
    if position == 'causal':
        kernel[:size//2] = 0  

    elif position == 'anticausal':
        kernel[size//2+1:] = 0  

    elif position == 'symmetric':
        pass

    kernel /= kernel.sum()
    return kernel


kernel_functions = {
    'increment': increment_kernel,
    'gaussian': gaussian_kernel,
    'gabor': gabor_kernel,
    'box': box_kernel,
    'box_HP': box_HP_kernel,
    'gaussian_HP': gaussian_HP_kernel,
    'box_BP': box_BP_kernel,
    'gaussian_BP': gaussian_BP_kernel,
    
}

kernel_types = kernel_functions.keys()

#%% 
# Computation


for sgn in signals:
    signal = eval(sgn)
    for scale1 in scales:
        for scale2 in scales:
            if scale1 == scale2:
                continue  # Avoid redundant computations

            for kernel_type in kernel_types:
                for position in positions:
                    key = (sgn, scale1, scale2, kernel_type, position)
                    
                    # Check if the result already exists
                    existing_result = load_results(key)
                    if existing_result:
                        results[key] = existing_result
                        continue  # Skip computation if already saved

                    # Call the kernel function for both scales
                    kernel1 = kernel_functions[kernel_type](scale1, position)
                    kernel2 = kernel_functions[kernel_type](scale2, position)

                    # Apply convolution for each kernel
                    signal_scale1 = convolve(signal, kernel1, mode='same')
                    signal_scale2 = convolve(signal, kernel2, mode='same')

                    max_lag = max(scale1, scale2) * max_lag_factor
                    te_values = []
                    te_std = []
                    for lag in range(1, max_lag + 1):
                        te_value = entropy.compute_TE(tools.reorder(signal_scale1), tools.reorder(signal_scale2), lag=lag, stride=lag)[0]
                        te_values.append(te_value)
                        te_std.append(entropy.get_last_info()[0])

                    # Save results immediately after computation
                    results[key] = te_values, te_std
                    save_results(key, te_values, te_std)
                    print(f'Saved result: {key}')


# %% 
# Plotting resulting TEs 

for kernel_type in kernel_types:
    fig, axes = plt.subplots(3, 3, figsize=(20, 22))  
    fig.suptitle(f'Transfer Entropy Analysis - {kernel_type.capitalize()}', fontsize=30)  

    for i, sgn in enumerate(signals):
        for j, position in enumerate(positions):
            ax = axes[i, j]
            ax.set_title(f'{sgn.capitalize()} - {position.capitalize()}', fontsize=26)  
            ax.set_xlabel('Lag', fontsize=24)
            ax.set_ylabel('Transfer Entropy', fontsize=24)
            
            for scale1 in scales:
                for scale2 in scales:
                    if scale1 == scale2:
                        continue  
                    key = (sgn, scale1, scale2, kernel_type, position)
                    te_values, te_std = results[key]
                    lags = range(1, len(te_values) + 1)
                    ax.errorbar(lags, te_values, yerr=te_std, label=f'Scale1={scale1}, Scale2={scale2}', capsize=5)
                    ax.axvline(x=scale1, color='k', linestyle=':')
                    ax.axvline(x=scale2, color='k', linestyle=':')
                    scale_diff = abs(scale1 - scale2)
                    ax.axvline(x=scale_diff, color='k', linestyle=':')

            ax.legend(fontsize=16)

    plt.tight_layout(rect=[0, 0, 1, 0.95]) 
    plt.savefig('/users2/local/e22froge/codes/TE_Filter/TE_filters_{}.pdf'.format(kernel_type), bbox_inches='tight')
    plt.show()


 # %% 
# Impulse Response Plotting

#kernel_types=['box','gaussian','gabor']
#kernel_types=['increment','box_HP','gaussian_HP']
kernel_types=['box_BP','gaussian_BP']


impulse = np.zeros(100)
impulse[len(impulse) // 2] = 1

fig, axes = plt.subplots(len(kernel_types), len(positions), figsize=(20, 22))
fig.suptitle(f'Impulse Response of Filters', fontsize=30)

for i, kernel_type in enumerate(kernel_types):
    for j, position in enumerate(positions):
        ax = axes[i, j]
        ax.set_title(f'{position.capitalize()} - {kernel_type.capitalize()}', fontsize=26)
        ax.set_xlabel('Time', fontsize=24)
        ax.set_ylabel('Amplitude', fontsize=24)
        for scale in scales:
            kernel = kernel_functions[kernel_type](scale, position)
            impulse_response = convolve(impulse, kernel, mode='same')
            ax.plot(impulse_response, label=f'Scale={scale}')
        ax.axhline(0, color='black', linestyle='--')  # Add horizontal line at 0
        ax.legend(fontsize=24)

plt.tight_layout(rect=[0, 0, 1, 0.95])
plt.savefig('/users2/local/e22froge/codes/TE_Filter/Impulse_responses_BP.pdf', bbox_inches='tight')
plt.show()

#%%
# Kernel Plotting
fig, axes = plt.subplots(len(kernel_types), len(positions), figsize=(20, 22))
fig.suptitle(f'Kernel of Filters', fontsize=30)

for i, kernel_type in enumerate(kernel_types):
    for j, position in enumerate(positions):
        ax = axes[i, j]
        ax.set_title(f'{position.capitalize()} - {kernel_type.capitalize()}', fontsize=26)
        ax.set_xlabel('Time', fontsize=24)
        ax.set_ylabel('Amplitude', fontsize=24)
        for scale in scales:
            kernel = kernel_functions[kernel_type](scale, position)
            ax.plot(kernel, label=f'Scale={scale}')
        ax.axhline(0, color='black', linestyle='--')  # Add horizontal line at 0
        ax.legend(fontsize=24)

plt.tight_layout(rect=[0, 0, 1, 0.95])
plt.savefig('/users2/local/e22froge/codes/TE_Filter/Kernels_BP.pdf', bbox_inches='tight')
plt.show()

# %% # %% 
# Plotting Signal Spectrum with Vertical Lines for Scales using scipy.welch and Gabor BP frequencies (log-log scale)
from scipy.signal import welch

fig, axes = plt.subplots(1, len(signals), figsize=(20, 10))

for i, sgn in enumerate(signals):
    signal = eval(sgn)
    ax = axes[i]
    
    # Compute the power spectral density using Welch's method
    freqs, psd = welch(signal, fs=1.0, nperseg=2048)  # fs=1.0 assumes normalized frequency
    
    # Plot the PSD on a log-log scale
    ax.plot(freqs, psd, label=f'{sgn.capitalize()} Spectrum')
    ax.set_xscale('log')
    ax.set_yscale('log')
    
    # Plot vertical lines corresponding to the scales
    for scale in scales:
        freq_scale = 1 / scale  # Frequency corresponding to each scale
        ax.axvline(x=freq_scale, color='r', linestyle='--', label=f'Scale={scale}')
    
    # Plot a dashed line for the difference between the scales
    scale_diff = abs(scales[1] - scales[0])
    freq_scale_diff = 1 / scale_diff
    ax.axvline(x=freq_scale_diff, color='b', linestyle='-.', label=f'Scale Difference={scale_diff}')
    
    # Add smaller dashed lines for Gabor modulation frequencies
    for scale in scales:
        omega_modulation = 5 / scale  # Modulation frequency used in Gabor filter
        ax.axvline(x=omega_modulation, color='g', linestyle=':', label=f'Gabor Modulation for Scale={scale}')

    ax.set_title(f'Spectrum of {sgn.capitalize()}', fontsize=24)
    ax.set_xlabel('Frequency (log scale)', fontsize=20)
    ax.set_ylabel('Power Spectral Density (log scale)', fontsize=20)
    ax.legend()

plt.tight_layout(rect=[0, 0, 1, 0.95])
#plt.savefig('/users2/local/e22froge/codes/TE_Filter/Signal_Spectrum.pdf', bbox_inches='tight')
plt.show()

# %%
# %% 
# Importing scipy's welch method
from scipy.signal import welch

# Plotting all Signal Spectrums on the same graph with Vertical Lines for Scales and Gabor Modulation

fig, ax = plt.subplots(figsize=(12, 8))
fig.suptitle('All Signal Spectrums with Vertical Lines for Scales and Gabor Modulation', fontsize=20)

colors = ['b', 'g', 'r','y']  # Define different colors for each signal
line_styles = ['-', '--', '-.',':']  # Line styles for different signals

for i, sgn in enumerate(signals):
    signal = eval(sgn)
    
    # Compute the power spectral density using Welch's method
    freqs, psd = welch(signal, nperseg=2048, fs=1.0)
    if sgn=='FBM_regu':
        psd=10**5*psd
    # Plot the power spectral density on the same axis
    ax.plot(freqs, psd, color=colors[i], linestyle=line_styles[i], label=f'{sgn.capitalize()}')
    ax.set_xscale('log')
    ax.set_yscale('log')
    # Plot vertical lines corresponding to the scales
    for scale in scales:
        freq_scale = 1 / scale  # Frequency corresponding to each scale
        ax.axvline(x=freq_scale, color='r', linestyle='--', label=f'Scale={scale}' if i == 0 else "")  # Add label only for the first signal
    
    # Plot a dashed line for the difference between the scales
    scale_diff = abs(scales[1] - scales[0])
    freq_scale_diff = 1 / scale_diff
    ax.axvline(x=freq_scale_diff, color='b', linestyle='-.', label=f'Scale Difference={scale_diff}' if i == 0 else "")  # Add label only once
    
    # Add smaller dashed lines for Gabor modulation frequencies
    for scale in scales:
        omega_modulation = 5 / scale  # Modulation frequency used in Gabor filter
        ax.axvline(x=omega_modulation, color='g', linestyle=':', label=f'Gabor Modulation for Scale={scale}' if i == 0 else "")

# Set titles and labels
ax.set_title('Power Spectral Density of Signals', fontsize=16)
ax.set_xlabel('Frequency', fontsize=14)
ax.set_ylabel('Power Spectral Density', fontsize=14)

# Display the legend
ax.legend()

# Display the plot
plt.tight_layout()
plt.savefig('/users2/local/e22froge/codes/TE_Filter/Signal_Spectrum_Superposed.pdf', bbox_inches='tight')
plt.show()

# %%
