import numpy as np
import matplotlib.pyplot as plt
from scipy.signal import *




#####
#ALL Filters applies along first dimension if multiple dimensions are given


####
#%%
def Box(signal1, tau1,  verbose = False):
    print(np.size(np.shape(signal1)))
    signal1=np.squeeze(signal1)
    Shape=list(np.shape(signal1))
    Result1=np.zeros(signal1.shape)
    #Result1[tau1:]=signal1[tau1:]-signal1[:-tau1]
    Shape[0]=2*tau1+1
    kernel = np.ones(Shape)/(2*tau1+1)
    #kernel[tau1]=1
    Result1 =  fftconvolve(signal1,kernel,mode='full',axes=0)[tau1:-tau1]
    fp=tau1
    lp=len(signal1)-tau1
    if verbose:
        plt.figure(1)
        plt.clf()
        #plt.subplot(121)
        plt.plot(signal1,'k.-')
        plt.plot(Result1,'r-')
        
    
    return Result1, fp, lp
#%%
def CausalBox(signal1, tau1,  verbose = False, order=[]):
    
    #Result1=np.zeros(signal1.shape)
    #Result1[tau1:]=signal1[tau1:]-signal1[:-tau1]
    signal1=np.squeeze(signal1)

    #Result1[tau1:]=signal1[tau1:]-signal1[:-tau1]

    Shape=list(np.shape(signal1))
    Shape[0]=tau1+1
    kernel = np.ones(Shape)/(tau1+1)
    #kernel[0]=1
    #Result1[tau1//2:] = fftconvolve(signal1,kernel,mode='full',axes=0)[tau1//2:len(signal1)]
    Result1 = fftconvolve(signal1,kernel,mode='full',axes=0)[:len(signal1)]
    fp=tau1
    lp=len(signal1)
        
    if verbose:
        plt.figure(1)
        #plt.clf()
        #plt.subplot(121)
        plt.plot(signal1,'k-')
        plt.plot(Result1,'r-')
        
    
    return Result1, fp, lp
#%%
def AntiCausalBox(signal1, tau1,  verbose = False):
    
    signal1=np.squeeze(signal1)

    Shape=list(np.shape(signal1))
    Shape[0]=tau1+1
    kernel = np.ones(Shape)/(tau1+1)
    #kernel[-1]=1
    Result1 = fftconvolve(signal1,kernel,mode='full',axes=0)[tau1:]
    fp=0
    lp=len(signal1)-tau1
    
    if verbose:
        plt.figure(1)
        plt.clf()
        #plt.subplot(121)
        plt.plot(signal1,'k-')
        plt.plot(Result1[:-tau1],'r-')
        


    return Result1, fp, lp
#%%
def Poor(signal1, tau1,  verbose = False):
    
    signal1=np.squeeze(signal1)
    Result1=np.zeros(signal1.shape)
    #Result1[tau1:]=signal1[tau1:]-signal1[:-tau1]

    Shape=list(np.shape(signal1))
    Shape[0]=2*tau1+1
    kernel = -np.ones(Shape)/(2*tau1)
    kernel[tau1]=1
    Result1 =  fftconvolve(signal1,kernel,mode='full',axes=0)[tau1:-tau1]
    fp=tau1
    lp=len(signal1)-tau1
    if verbose:
        plt.figure(1)
        plt.clf()
        #plt.subplot(121)
        plt.plot(signal1,'k.-')
        plt.plot(Result1,'r-')
        
    
    return Result1, fp, lp

#%%
def CausalPoor(signal1, tau1,  verbose = False, order=[]):
    
    #Result1=np.zeros(signal1.shape)
    #Result1[tau1:]=signal1[tau1:]-signal1[:-tau1]
    Shape=list(np.shape(signal1))
    Shape[0]=tau1+1
    kernel = -np.ones(Shape)/tau1
    kernel[0]=1
    #Result1[tau1//2:] = fftconvolve(signal1,kernel,mode='full',axes=0)[tau1//2:len(signal1)]
    Result1 = fftconvolve(signal1,kernel,mode='full',axes=0)[:len(signal1)]
    fp=tau1
    lp=len(signal1)
        
    if verbose:
        plt.figure(1)
        #plt.clf()
        #plt.subplot(121)
        plt.plot(signal1,'k-')
        plt.plot(Result1,'r-')
        
    
    return Result1, fp, lp
#%%
def AntiCausalPoor(signal1, tau1,  verbose = False):
    
    Shape=list(np.shape(signal1))
    Shape[0]=tau1+1
    kernel = -np.ones(Shape)/tau1
    kernel[-1]=1
    Result1 = fftconvolve(signal1,kernel,mode='full',axes=0)[tau1:]
    fp=0
    lp=len(signal1)-tau1
    
    if verbose:
        plt.figure(1)
        plt.clf()
        #plt.subplot(121)
        #plt.plot(signal1,'k-')
        plt.plot(Result1[:-tau1],'r-')
        


    return Result1, fp, lp

#%%
def Incr(signal1, tau1,  verbose = False):
    signal1=np.squeeze(signal1)
    N=np.shape(signal1)[0]
    Result1=np.zeros(signal1.shape)
    Result1[tau1:] = signal1.take(indices=range(tau1,N),axis=0)-signal1.take(indices=range(0,N-tau1),axis=0)
    fp=tau1
    lp=len(signal1)
    if verbose:
        plt.figure(1)
        plt.clf()
        #plt.subplot(121)
        plt.plot(signal1,'k.-')
        plt.plot(Result1,'r-')
        


    return Result1, fp, lp


def AntiIncr(signal1, tau1,  verbose = False):
    signal1=np.squeeze(signal1)
    N=np.shape(signal1)[0]
    Result1=np.zeros(signal1.shape)
    Result1[:-tau1] = -signal1.take(indices=range(tau1,N),axis=0)+signal1.take(indices=range(0,N-tau1),axis=0)
    fp=0
    lp=len(signal1)-tau1
    if verbose:
        plt.figure(1)
        plt.clf()
        #plt.subplot(121)
        plt.plot(signal1,'k.-')
        plt.plot(Result1,'r-')
        


    return Result1, fp, lp

#%%
def SymIncr(signal1, tau1,  verbose = False):
    signal1=np.squeeze(signal1)
    N=np.shape(signal1)[0]
    Result1=np.zeros(signal1.shape)
    Result1[tau1:-tau1] = 2*signal1.take(indices=range(tau1,N-tau1),axis=0)-signal1.take(indices=range(0,N-2*tau1),axis=0)-signal1.take(indices=range(2*tau1,N),axis=0)
    
    Result1[tau1:-tau1] = 2*signal1[tau1:-tau1]-signal1[:-2*tau1]-signal1[2*tau1:]
    
    fp=tau1
    lp=len(signal1)-tau1
    if verbose:
        plt.figure(1)
        plt.clf()
        #plt.subplot(121)
        plt.plot(signal1,'k.-')
        plt.plot(Result1,'r.-')
    

    return Result1, fp, lp

def LPT(signal1, tau1,  verbose = False, order=[]): #Same as Box
    
    #Result1=np.zeros(signal1.shape)
    #Result1[tau1:]=signal1[tau1:]-signal1[:-tau1]
    signal1=np.squeeze(signal1)

    #Result1[tau1:]=signal1[tau1:]-signal1[:-tau1]

    Shape=list(np.shape(signal1))
    Shape[0]=tau1+1
    kernel = np.ones(Shape)/(tau1+1)
    #kernel[0]=1
    #Result1[tau1//2:] = fftconvolve(signal1,kernel,mode='full',axes=0)[tau1//2:len(signal1)]
    Result1 = fftconvolve(signal1,kernel,mode='full',axes=0)[:len(signal1)]
    fp=tau1
    lp=len(signal1)
        
    if verbose:
        plt.figure(1)
        #plt.clf()
        #plt.subplot(121)
        plt.plot(signal1,'k-')
        plt.plot(Result1,'r-')
        
    
    return Result1, fp, lp

def HPT(signal1, tau1,  verbose = False, order=[]):
    fp=tau1
    lp=len(signal1)
    Result1=signal1-LPT(signal1,tau1)[0]
    if verbose:
        plt.figure(1)
        #plt.clf()
        #plt.subplot(121)
        plt.plot(signal1,'k-')
        plt.plot(Result1,'r-')
        
    return Result1, fp, lp

def BPT(signal1, tau1, tau2, verbose = False, order=[]):
    fp=tau1
    lp=len(signal1)
    Result1=LPT(signal1,tau1)[0]-LPT(signal1,tau2)[0]
    if verbose:
        plt.figure(1)
        #plt.clf()
        #plt.subplot(121)
        plt.plot(signal1,'k-')
        plt.plot(Result1,'r-')
        
    return Result1, fp, lp


def BPFreq(signal1,tau1,tau2,verbose=False):
    signal1=np.squeeze(signal1)
    Result1=np.zeros(signal1.shape)
    
    fp=tau1
    lp=len(signal1)-tau1

    minfreq=1/tau2
    maxfreq=1/tau1
    fftsignal = np.fft.fft(signal1,axis=0)
    freq = np.fft.fftfreq(np.size(signal1,axis=0))
    Mask=(np.abs(freq)>=minfreq).astype(int)*(np.abs(freq)<=maxfreq).astype(int)
    
    fftfiltered=(fftsignal.T*Mask).T
    Result1 = np.fft.ifft(fftfiltered,axis=0)
    if verbose:
        plt.figure(1)
        plt.clf()
        #plt.subplot(121)
        plt.plot(signal1,'k.-')
        plt.plot(Result1,'r-')
    


    return Result1, fp, lp

def BPOrder(signal1,tau1,tau2,order=2,verbose=False):
    signal1=np.squeeze(signal1)
    Result1=np.zeros(signal1.shape)
    
    fp=tau1
    lp=len(signal1)-tau1

    minfreq=1/tau2
    maxfreq=1/tau1
    fftsignal = np.fft.fft(signal1,axis=0)
    freq = np.fft.fftfreq(np.size(signal1,axis=0))
    MaskHP=(1j*(freq/minfreq)**order)/(1+1j*(freq/minfreq)**order) 
    MaskLP=1/(1+1j*(freq/maxfreq)**order) 
    Mask=MaskLP*MaskHP
    fftfiltered=(fftsignal.T*Mask).T
    Result1 = np.fft.ifft(fftfiltered,axis=0)
    if verbose:
        plt.figure(1)
        plt.clf()
        #plt.subplot(121)
        plt.plot(signal1,'k.-')
        plt.plot(Result1,'r-')


    return Result1, fp, lp
    
