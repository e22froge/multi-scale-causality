#%%
from EwenLib import Filters
import numpy as np
import matplotlib.pyplot as plt

#Filters lists
Filts=['LPT','HPT','Box','CausalBox','AntiCausalBox','Poor','CausalPoor','AntiCausalPoor','Incr','AntiIncr','SymIncr']
FiltsBP=['BPOrder','BPFreq','BPT']


#%% Test on dirak

for filt in Filts:

    print(filt+ ' 2D')
    N=250
    test= np.zeros((N,2))
    test[N//2,:]=1
    sig=eval('Filters.'+filt+'(test,50,verbose=True)[0]')
    plt.show()

    print(filt+ ' 1D')
    N=250
    test= np.zeros(N)
    test[N//2]=1
    sig=eval('Filters.'+filt+'(test,50,verbose=True)[0]')
    plt.show()

for filt in FiltsBP:
    print(filt+ ' 2D')
    N=250
    test= np.zeros((N,2))
    test[N//2,:]=1
    sig=eval('Filters.'+filt+'(test,50,100,verbose=True)[0]')
    plt.show()

    print(filt+ ' 1D')
    N=250
    test= np.zeros(N)
    test[N//2]=1
    sig=eval('Filters.'+filt+'(test,50,100,verbose=True)[0]')
    plt.show()

#%% Test on arange

for filt in Filts:
    print(filt)
    N=250
    test= np.arange(N)
    sig=eval('Filters.'+filt+'(test,50,verbose=True)[0]')
    plt.show()

for filt in FiltsBP:
    print(filt)
    N=250
    test= np.arange(N)
    sig=eval('Filters.'+filt+'(test,50,100,verbose=True)[0]')
    plt.show()

# %%
