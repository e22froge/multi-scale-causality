import numpy as np
import matplotlib.pyplot as plt
from sklearn.neighbors import NearestNeighbors

def create_database(data, p, overlapping=False):
    """
    Create a database of p continuous values with or without overlapping.

    Parameters:
    data (array-like): Input data series.
    p (int): The size of the tuples.
    overlapping (bool): Whether the tuples should overlap.

    Returns:
    np.ndarray: The database of tuples.
    """
    # Create sliding window view of the data with window size p+1
    window = np.lib.stride_tricks.sliding_window_view(data, window_shape=(p,))
    # Set stride based on overlapping flag
    stride = 1 if overlapping else p
    # Select windows based on the stride
    db = window[0::stride, :]
    return db


def prediction_neighbors(db_data, db_analog, k, weighting=True, verbose=False, Random=False, NoLR=False, normalize=False):
    """
    Predict future data points using nearest neighbors algorithm.

    Parameters:
    data (array-like): Input data series.
    db_analog (array-like): Database of tuples.
    k (int): Number of neighbors to use.
    weighting (bool): Whether to weight the neighbors.
    verbose (bool): Whether to print verbose output.
    Random (bool): Whether to use random neighbors.
    NoLR (bool): Whether to skip linear regression for prediction.
    normalize (bool): Whether to normalize the data.

    Returns:
    np.ndarray: The predicted values.
    """
    Nanalog , pp1 = db_analog.shape
    Ndata,p = db_data.shape


    # Check if db_data is of size p and db_analog is of size p+1
    if db_analog.shape[1] != p + 1:
        raise ValueError(f"Expected db_analog of size p+1={p + 1} (analogs + successors), but got size {db_analog.shape[1]}")



    if normalize:
        # Compute means for detrending
        db_data_means = np.mean(db_data, axis=1)
        # Detrend db_data by subtracting mean
        db_data -= db_data_means[:, np.newaxis]

        db_means = np.mean(db_analog[:, :-1], axis=1)
        # Detrend database by subtracting mean
        db_analog -= db_means[:, np.newaxis]

    # Initialize and fit nearest neighbors model on the database (excluding the last column)
    neigh = NearestNeighbors(n_jobs=4)
    neigh.fit(db_analog[:, :-1])
    
    # Find k nearest neighbors for each p-uplet
    Dist, Idx = neigh.kneighbors(db_data, n_neighbors=k, return_distance=True)

    if Random:
        # Replace indices with random indices if Random flag is set
        Idx = np.random.randint(low=0, high=Nanalog, size=Idx.shape)
    
    if weighting:
        # Compute weights based on distances
        med = np.median(Dist, axis=1)
        weights = np.exp(-Dist / med[:, np.newaxis])
        weights /= np.sum(weights, axis=1)[:, np.newaxis]  # Normalize weights
    else:
        weights = np.ones_like(Dist)  # Equal weights if not weighting

    vals = np.full((Ndata+p-1),np.nan)  # Initialize prediction array with NaNs

    if NoLR:
        # Simple weighted average without linear regression
        vals[p:] = np.sum(weights * db_analog[Idx, -1], axis=1)[:-1]
    else:
        # Linear regression to predict values
        X = db_analog[Idx, :-1]  # Nearest neighbor features
        y = (weights * db_analog[Idx, -1])[:, :, np.newaxis]  # Weighted target values

        # Add bias term to features
        X = np.pad(X, [(0, 0), (0, 0), (1, 0)], mode='constant', constant_values=1)

        # Compute regression coefficients
        coef = np.linalg.inv(np.transpose(X, axes=[0, 2, 1]) @ (weights[:, :, np.newaxis] * X)) @ np.transpose(X, axes=[0, 2, 1]) @ y
        # Predict values using the regression coefficients
        vals[p:] = coef[:-1, 0, 0] + np.sum(coef[:, 1:, 0] * db_data, axis=1)[:-1]
    
    if normalize:
        # Re-add the mean to the predictions if data was normalized
        vals[p:] += db_data_means[:-1]
    
    if verbose:
        print('Index', Idx)
        print('Dist', Dist)
        print('Puplets', db_data)
        
        for _ in range(5):
            t = 0
            # Plot the nearest neighbors and the prediction
            
            data=db_data[:,-1]
            for i in range(k):
                plt.plot(db_analog[Idx[t, i]], c='blue', alpha=np.min(Dist[t, :]) / Dist[t, i])
            plt.plot(data[t:t + p + 1], c='green')
            plt.plot([p - 1, p], [data[t + p - 1], vals[t + p]], c='red')
            plt.show()

    return vals
